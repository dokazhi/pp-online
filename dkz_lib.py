import requests
from lxml import etree,builder
import asyncio


async def sendDoc(req):
    import psycopg2
    import json
    from datetime import datetime
    json_file = open('mimetypes.json',"r")
    mimetypes = json.load(json_file)
    json_file.close()
    with open("config.json") as config_file:
            config = json.load(config_file)
    con = psycopg2.connect(
            dbname=config["dbname"],
            user=config["user"],
            password=config["password"],
            host=config["host"],
            port=config["port"]
        )
    now_y = datetime.now().year
    now_m = datetime.now().month
    pack_reference = int(str(now_m) + ""+str(now_y))
    responses = []
    i=0
    parser = etree.XMLParser(remove_blank_text=True)
    for name in req.files:
        i+=1
        if(req.files[name].type in mimetypes):
            try:
                fileName = await (saveFile(req.files[name],mimetypes))
                print(fileName)
                cur = con.cursor()
                cur.execute("""
                INSERT INTO pps_docs(nametxt,pack_reference,owner_bin,receiver_bin,type_delegate,receiver_mail,partner_id)
                VALUES(%s,%s,%s,%s,%s,%s,%s) RETURNING id
                """,(
                    req.form['nametxt'],
                    str(pack_reference),
                    str(170340017345),
                    req.form['receiver_bin'],
                    req.form['type_delegate'],
                    req.form['receiver_mail'],
                    req.form['partner_id']
                ))
                con.commit()
                u_ref=cur.fetchone()[0]
                url="https://www.podpishi.kz/api.php"
                data = {
                    "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
                    "login":"malik@unie.kz",
                    "nametxt":req.form['nametxt']+" part("+str(i)+")",
                    "uniq_reference":u_ref,
                    "pack_reference":pack_reference,
                    "owner_bin":str(170340017345),
                    "receiver_bin":req.form['receiver_bin'],
                    "type_delegate":req.form['type_delegate'],
                    "receiver_mail":req.form['receiver_mail']
                }
                files = {
                    'uf':open(fileName,"rb")
                }
                res = requests.post(url=url,files=files,data=data)
                import os
                os.remove(fileName)
                
                if(res.status_code==200):
                    xml = etree.XML(res.content,parser)
                    print(xml[0].tag)
                    pid = 0
                    created = 0
                    index = 0
                    print(res.text)
                    if xml[0].text == "ok":
                        pid = int(xml[0].attrib["num"])
                        created = int(xml[1][0].attrib["created"])
                        index1 = int(xml[1][0].attrib["i"])
                        index2 = int(xml[1][1].attrib["i"])
                        cur.execute("""
                                update pps_docs set (pid,p_created,pindex1,pindex2)= (%s,%s,%s,%s)
                                where id = %s
                                """,(pid,created,index1,index2,u_ref))
                        responses.append({
                                    "id":u_ref,
                                    "pid":pid,
                                    "name":req.files[name].name,
                                    "status":bool(int(cur.statusmessage[len(cur.statusmessage)-1]))
                        })
                    else:
                        cur.execute("""delete from pps_docs where id=%s""",(u_ref,))
                    con.commit()
            except BaseException as e:
                print(e)
                continue
            finally:
                cur.close()
    else:
        con.close()
    return responses
    

async def getDocStatus(id):
    import psycopg2
    import json
    import base64
    with open("config.json") as config_file:
            config = json.load(config_file)
    con = psycopg2.connect(
            dbname=config["dbname"],
            user=config["user"],
            password=config["password"],
            host=config["host"],
            port=config["port"]
        )
    cur=con.cursor()
    cur.execute("""
    SELECT pid,p_created from pps_docs
    where id = %s
    """,(id,))
    
    row = cur.fetchone()
    
    cur.close()
    con.close()
    xml=builder.E.data(
        builder.E.oper("getstatus"),
        builder.E.docuniq(str(row[0])),
        builder.E.time(row[1])
    )
    etree.dump(xml)
    xml_str = etree.tostring(xml,pretty_print=True,encoding="UTF-8")
    enc = base64.b64encode(xml_str)
    
    url="https://api.podpishi.kz/apistatus.php"
    data = {
        "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
        "login":"malik@unie.kz",
        "command":enc
    }
    resp=requests.post(url=url,data=data)
    
    parser = etree.XMLParser(remove_blank_text=False)
    xml = etree.XML(resp.content,parser)
    data={}
    if(xml[0].tag!="status"):
        data['error']=False
        data['signers']=[]
        for element in xml[0][1]:
            print(element.attrib["signed"])
            data['signers'].append({
                
                "email":element.attrib['email'],
                "signed":bool(int(element.attrib["signed"]))
            })
        return data    
    else:
        return {
            "error":True,
            "msg":"resp error"
        }    
    

async def getDoc(id):
    import psycopg2
    import json
    import base64
    with open("config.json") as config_file:
            config = json.load(config_file)
    con = psycopg2.connect(
            dbname=config["dbname"],
            user=config["user"],
            password=config["password"],
            host=config["host"],
            port=config["port"]
        )
    cur=con.cursor()
    cur.execute("""
    SELECT pid,p_created from pps_docs
    where id = %s
    """,(id,))
    
    row = cur.fetchone()
    
    cur.close()
    con.close()
    xml=builder.E.data(
        builder.E.oper("getdoc"),
        builder.E.docnum(str(row[0])),
        builder.E.time(row[1])
    )
    etree.dump(xml)
    xml_str = etree.tostring(xml,pretty_print=True,encoding="UTF-8")
    enc = base64.b64encode(xml_str)
    
    url="https://api.podpishi.kz/apistatus.php"
    data = {
        "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
        "login":"malik@unie.kz",
        "command":enc
    }
    resp=requests.post(url=url,data=data)
    try:
        parser = etree.XMLParser(remove_blank_text=False)
        xml = etree.XML(resp.content,parser)
        if(xml.attrib['err']=="yes"):
            return {
                "error":True,
                "msg":xml[1].text
            }
    except BaseException as e:
        return{
            "error":False,
            "data":resp.content
        }

async def getDocumentList(date1,date2):
    import base64
    xml=builder.E.data(
        builder.E.oper("getlistdocs"),
        builder.E.time1('20180105100500'),
        builder.E.time2('20180929100500'),
        builder.E.owner_bin(str(170340017345)),
        builder.E.time('20180924101100')
    )
    etree.dump(xml)
    xml_str = etree.tostring(xml,pretty_print=True,encoding="UTF-8")
    enc = base64.b64encode(xml_str)
    
    url="https://api.podpishi.kz/apistatus.php"
    data = {
        "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
        "login":"malik@unie.kz",
        "command":enc
    }
    resp=requests.post(url=url,data=data)
    print(resp.text)
    return ""
        


async def getAvailDocs():
    import datetime
    import base64
    xml = builder.E.data(
        builder.E.oper('getavailabledocs'),
        builder.E.owner_bin("170340017345"),
        builder.E.time("{0:%Y%m%d%H%M%S}".format(datetime.datetime.now()))
    )
    xml_str = etree.tostring(xml,pretty_print=True,encoding="UTF-8")
    enc = base64.b64encode(xml_str)
    url="https://api.podpishi.kz/apistatus.php"
    data = {
        "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
        "login":"malik@unie.kz",
        "command":enc
    }
    res = requests.post(url=url,data=data)
    print(res.text)
    parser = etree.XMLParser(remove_blank_text=False)
    xml = etree.XML(res.content,parser)
    if(xml.attrib['err']=="no"):
        return{
            "error":False,
            "count":int(xml.attrib["counts"])
        }
    else:
        return{
            "error":True,
            "message":xml[1].text
        }
    
    

async def getTarifs():
    import datetime
    import base64
    xml = builder.E.data(
        builder.E.oper('gettarifplan'),
        builder.E.time("{0:%Y%m%d%H%M%S}".format(datetime.datetime.now()))
    )
    xml_str = etree.tostring(xml,pretty_print=True,encoding="UTF-8")
    enc = base64.b64encode(xml_str)
    url="https://api.podpishi.kz/apistatus.php"
    data = {
        "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
        "login":"malik@unie.kz",
        "command":enc
    }
    res = requests.post(url=url,data=data)
    parser = etree.XMLParser(remove_blank_text=False)
    xml = etree.XML(res.content,parser)
    if(xml.attrib["err"]=="no"):
        data = []
        for i in range(len(xml)):
            if(xml[i].tag=="tarif"):
                data.append({
                    "no":xml[i].text,
                    "name":xml[i+1].text,
                    "counts":xml[i+2].text,
                    "price":xml[i+3].text
                })
            else:
                continue
        return{
            "error":False,
            "data":data
        }
    else:
        return{
            "error":True,
            "msg":xml[1].text
        }
    

async def addSign(id,sign):
    import datetime
    import psycopg2
    import json
    import base64
    with open("config.json") as config_file:
            config = json.load(config_file)
    con = psycopg2.connect(
            dbname=config["dbname"],
            user=config["user"],
            password=config["password"],
            host=config["host"],
            port=config["port"]
        )
    cur=con.cursor()
    cur.execute("""
    SELECT pid,p_created,pindex2 from pps_docs
    where id = %s
    """,(id,))
    
    row = cur.fetchone()
    print(row)
    
    xml = builder.E.data(
        builder.E.oper('addsignature'),
        builder.E.docnum(str(row[0])),
        builder.E.indexnum(str(row[2])),
        builder.E.signature_pem(str(base64.b64encode(sign.encode("utf-8")))),
        builder.E.time("{0:%Y%m%d%H%M%S}".format(datetime.datetime.now()))

    )
    xml_str = etree.tostring(xml,pretty_print=True,encoding="UTF-8")
    enc = base64.b64encode(xml_str)
    url="https://api.podpishi.kz/apistatus.php"
    data = {
        "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
        "login":"malik@unie.kz",
        "command":enc
    }
    res = requests.post(url=url,data=data)
    print(res.text)
    return ""



async def delPermissions(id):
    import datetime
    import psycopg2
    import json
    import base64
    with open("config.json") as config_file:
            config = json.load(config_file)
    con = psycopg2.connect(
            dbname=config["dbname"],
            user=config["user"],
            password=config["password"],
            host=config["host"],
            port=config["port"]
        )
    cur=con.cursor()
    cur.execute("""
    SELECT pid,p_created,pindex2 from pps_docs
    where id = %s
    """,(id,))
    row = cur.fetchone()
    xml = builder.E.data(
        builder.E.oper('delpermission'),
        builder.E.docnum(str(row[0])),
        builder.E.indexnum(str(row[2])),
        builder.E.time("{0:%Y%m%d%H%M%S}".format(datetime.datetime.now()))

    )
    xml_str = etree.tostring(xml,pretty_print=True,encoding="UTF-8")
    enc = base64.b64encode(xml_str)
    url="https://api.podpishi.kz/apistatus.php"
    data = {
        "apikey":"dcadc446a2bf92d1fc83163aa4c53a9b",
        "login":"malik@unie.kz",
        "command":enc
    }
    res = requests.post(url=url,data=data)
    print(res.text)
    parser = etree.XMLParser(remove_blank_text=False)
    xml = etree.XML(res.content,parser)
    if(xml.attrib["err"]=="no"):
        return {
            "error":False,
            "msg":"Success"
        }
    else:
        return {
            "error":True,
            "msg":"Failure"
        }
    




async def saveFile(reqFile,mime):
    import time
    import random
    
    
    random.seed(int(time.time()))
    newFileName =str(random.randint(0,int(time.time())))+str()+mime[reqFile.type] 
    newFile=open(newFileName,"wb")
    newFile.write(reqFile.body)
    newFile.close()
    return newFileName
    
    
    


if __name__=="__main__":
    print("Sorry is a not executable file")