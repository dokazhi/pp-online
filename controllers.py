import dkz_lib

async def sendDocument(request):
    
    data = await dkz_lib.sendDoc(req=request);
    return request.Response(json=data)


async def getDocumentStatus(request):
    try:
        data = request.json
        ret = await dkz_lib.getDocStatus(id=data["id"])
        print(ret)
        if ret['error']:
            return request.Response(code=400,json=ret)
        else:
            return request.Response(code=200,json=ret)
        
    except BaseException as e:
        print(e)
        return request.Response(code=400,text="bad data")
    

async def getDocument(request):
    try:
        print(request.query)
        # data = request.json
        ret = await dkz_lib.getDoc(id=request.query["id"])
        print(type(ret))
        if(ret["error"]):
            return request.Response(code=400,json=ret)
        else:
            return request.Response(mime_type="application/zip",body=ret["data"])
    except BaseException as e:
        print(e)
        return request.Response(code=400,text=e)

async def getDocumentList(request):
    try:
        print(request)
        data = await dkz_lib.getDocumentList(123,123)
        return request.Response(code=200,text="fine")
    except BaseException as e:
        return request.Response(code = 400,text=e)


async def getAvailableDocs(request):
    try:
        print(request)
        data = await dkz_lib.getAvailDocs()
        if (data["error"]):
            return request.Response(code=400,text=data["msg"])
        else:
            return request.Response(code=200,json=data)
    except BaseException as e:
        print(e)
        return request.Response(code=400,text=e)

async def getTarifPlan(request):
    try:
        data = await dkz_lib.getTarifs()
        if (data["error"]):
            return request.Response(code=400,text=data["msg"])
        else:
            return request.Response(code=200,json=data["data"])
    except BaseException as e:
        print(e)
        return request.Response(code=400,text=e)

async def addSign(request):
    try:
        data = request.json
        print(data)
        resp = await dkz_lib.addSign(data["id"],data["sign"])
        return request.Response(code =200,text="Nice")
    except BaseException as e:
        print(e)
        return request.Response(code=400,text=e)

async def delPerms(request):
    try:
        data = request.json
        res = await dkz_lib.delPermissions(data["id"])
        if (res["error"]):
            return request.Response(code=400,json=res)
        else:
            return request.Response(code=200,json=res)
    except BaseException as e:
        print(e)
        return request.Response(code=400,text=e)

if __name__=="__main__":
    print("Sorry not today")