from japronto import Application
import controllers

# Views handle logic, take request as a parameter and
# returns Response object back to the client
def hello(request):
    return request.Response(text='Hello world!')


# The Application instance is a fundamental concept.
# It is a parent to all the resources and all the settings
# can be tweaked here.
app = Application()

# The Router instance lets you register your handlers and execute
# them depending on the url path and methods
app.router.add_route('/', hello)
app.router.add_route('/doc',controllers.sendDocument)
app.router.add_route("/doc_status",controllers.getDocumentStatus)
app.router.add_route('/doc_get',controllers.getDocument,'GET')
app.router.add_route('/doc_list',controllers.getDocumentList)
app.router.add_route("/doc_avail",controllers.getAvailableDocs)
app.router.add_route('/doc_tarifs',controllers.getTarifPlan)
app.router.add_route('/doc_sign',controllers.addSign)
app.router.add_route("/doc_del",controllers.delPerms)
# Finally start our server and handle requests until termination is
# requested. Enabling debug lets you see request logs and stack traces.
app.run(host="0.0.0.0", port=8888,debug=True)