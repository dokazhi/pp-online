FROM python:alpine3.6
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev build-base
RUN apk add --update  g++ gcc libxslt-dev
COPY . /app
WORKDIR /app
RUN python --version
RUN pip --version

RUN pip install -r requirements.txt

RUN pip --no-cache install https://github.com/squeaky-pl/japronto/archive/master.zip 
RUN pip install --upgrade pip
EXPOSE 8888
CMD python ./hello.py